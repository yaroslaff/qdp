# F1 Deploy

Tool to quick and automatical deploy site-packages (in f1deploy format)

F1Deploy does:

- Download site package
- Unpack it on disk
- Enable apache virtual sites
- Change Dynamic DNS records (Cloudflare)
- Get LetsEncrypt certificates

All of this is done in one command (example):
~~~
f1deploy --setup http://sysattack.com/f1repo/sysattack-tk.tar.gz
~~~

## Installation

Download repo: 
~~~
git clone https://gitlab.com/yaroslaff/f1deploy.git
~~~

Make local f1d directory: 
~~~
mkdir /var/www/f1deploy
~~~

Edit `/etc/apache2/conf-available/f1deploy.conf`:
~~~
IncludeOptional /var/www/f1deploy/sites-enabled/*.conf
~~~

Enable it: 
~~~
a2enconf f1deploy
systemctl reload apache2
~~~

